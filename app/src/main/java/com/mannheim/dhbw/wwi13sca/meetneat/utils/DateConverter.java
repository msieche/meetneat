package com.mannheim.dhbw.wwi13sca.meetneat.utils;

import android.text.format.DateFormat;

import com.mannheim.dhbw.wwi13sca.meetneat.App;

import java.sql.Timestamp;
import java.util.Date;

public class DateConverter {
    public static Date getDateFromTimestamp(long timestamp) {
        Timestamp stamp = new Timestamp(timestamp);
        return new Date(stamp.getTime());
    }

    public static String getLocalizedDateString(Date date) {
        return DateFormat.getMediumDateFormat(App.context).format(date) + " " +  DateFormat.getTimeFormat(App.context).format(date);
    }

    public static long getUnixTimestamp(Date date) {
        return date.getTime() / 1000;
    }

    public static boolean isPastDate(Date date) {
        Date dateNow = new Date();
        return date.before(dateNow);
    }
}
