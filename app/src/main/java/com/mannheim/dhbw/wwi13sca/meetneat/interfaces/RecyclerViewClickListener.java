package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

import android.view.View;

public interface RecyclerViewClickListener {
    void recyclerViewListClicked(View view, int position);
}
