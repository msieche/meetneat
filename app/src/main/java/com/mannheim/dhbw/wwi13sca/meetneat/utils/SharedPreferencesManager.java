package com.mannheim.dhbw.wwi13sca.meetneat.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;

public class SharedPreferencesManager {
    private static final String TAG = "SharedPreferencesManager";

    private static SharedPreferencesManager instance;
    private SharedPreferences sharedPreferences;

    private SharedPreferencesManager() {
        sharedPreferences = App.context.getSharedPreferences(SharedPreferencesKeys.PREFERENCES, Context.MODE_PRIVATE);
    }

    public static synchronized SharedPreferencesManager getInstance () {
        if (SharedPreferencesManager.instance == null) {
            Log.i(TAG, "instantiating SharedPreferencesManager");
            SharedPreferencesManager.instance = new SharedPreferencesManager();
        }
        return SharedPreferencesManager.instance;
    }

    public void clearCurrentUser() {
        Log.i(TAG, "clear current user");

        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_PHONENUMBER).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_BIRTHYEAR).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_FIRSTNAME).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_LASTNAME).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_CITY).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_UID).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_GENDER).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_HABITS).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_INFO).apply();
        sharedPreferences.edit().remove(SharedPreferencesKeys.CURRENT_USER_PROFILEPICTURE).apply();
    }

    public void removeEntry(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public String getStringWithDefaultValue(String key, String defaultValue)  {
        return sharedPreferences.getString(key, defaultValue);
    }

    public void addString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void addCurrentUser(UserProfile currentUser) {
        Log.i(TAG, "add current user");

        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_PHONENUMBER, currentUser.getPhonenumber());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_FIRSTNAME, currentUser.getFirstname());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_LASTNAME, currentUser.getLastname());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_CITY, currentUser.getCity());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_BIRTHYEAR, currentUser.getBirthyear());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_GENDER, currentUser.getGender());
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_PROFILEPICTURE, currentUser.getProfilePicture());
    }
}


