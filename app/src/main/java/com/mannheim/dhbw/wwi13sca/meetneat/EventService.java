package com.mannheim.dhbw.wwi13sca.meetneat;

import android.support.annotation.NonNull;
import android.util.Log;

import com.kumulos.android.jsonclient.ResponseHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosCreateEventInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosEventManagerInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosFetchEventInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.network.KumulosQuery;
import com.mannheim.dhbw.wwi13sca.meetneat.network.KumulosQueryManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class EventService implements KumulosCreateEventInterface, KumulosFetchEventInterface, KumulosEventManagerInterface {
    private static final String TAG = "EventService";

    public enum FetchMode {
        ALL_FUTURE_EVENTS,
        ALL_HOSTED_EVENTS,
        ALL_JOINED_EVENTS
    }

    public interface CreateEventCallbackHandler {
        void onCreateEventSuccess();
        void onCreateEventError(String errorMessage);
    }

    public interface FetchEventCallbackHandler {
        void onFetchEventSuccess(ArrayList<Event> events);
        void onFetchEventError(String errorMessage);
    }

    private static EventService eventService;

    private CreateEventCallbackHandler createEventCallbackHandler;
    private EventService() {}

    public static EventService getInstance() {
        if (eventService == null) {
            eventService = new EventService();
        }
        return eventService;
    }

    public void setCreateEventCallbackHandler(CreateEventCallbackHandler createEventCallbackHandler) {
        this.createEventCallbackHandler = createEventCallbackHandler;
    }

    public void createEvent(String title, String date, String location, String description, String price, String picture, String hostName, boolean[] eventAttributes, String minGuests, String maxGuests) {
        createEvent(title, date, location, description,
                eventAttributes[0] ? "1" : "0",
                eventAttributes[1] ? "1" : "0",
                eventAttributes[2] ? "1" : "0",
                eventAttributes[3] ? "1" : "0",
                eventAttributes[4] ? "1" : "0",
                price,
                picture,
                hostName,
                minGuests,
                maxGuests);
    }

    @Override
    public void createEvent(String title, String date, String location, String description, String isVeggie, String isVegan, String isGlutenfree, String isMenOnly, String isWomenOnly, String price, String picture, String hostName, String minGuests, String maxGuests) {
        Log.i(TAG, "start to create a new event");

        if (createEventCallbackHandler == null) {
            Log.e(TAG, "no createEventCallbackHandler configured, abort create event");
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("title", title);
        params.put("date", date);
        params.put("location", location);
        params.put("description", description);
        params.put("isVeggie", isVeggie);
        params.put("isVegan", isVegan);
        params.put("isGlutenfree", isGlutenfree);
        params.put("isMenOnly", isMenOnly);
        params.put("isWomenOnly", isWomenOnly);
        params.put("price", price);
        params.put("picture", picture);
        params.put("hostName", hostName);
        params.put("host", UserProfile.getInstance().getUserId());
        params.put("minGuests", minGuests);
        params.put("maxGuests", maxGuests);

        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.createEvent, params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                Log.i(TAG, "successfully created a new event");

                int eventId = (int) result;

                createEventCallbackHandler.onCreateEventSuccess();
            }
        });
    }

    @Override
    public void fetchEvents(@NonNull FetchMode fetchMode, @NonNull final FetchEventCallbackHandler callbackHandler) {
        Log.i(TAG, "start to fetch events");

        HashMap<String, String> params = new HashMap<>();
        params.put("host", UserProfile.getInstance().getUserId());
        params.put("date", String.valueOf(new Date().getTime()/1000));


        KumulosQueryManager.getInstance().executeQuery(KumulosQueryManager.getQuery(fetchMode), params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                ArrayList<LinkedHashMap<String, Object>> objects = (ArrayList<LinkedHashMap<String,Object>>) result;
                ArrayList<Event> events = new ArrayList<>();
                for (LinkedHashMap<String, Object> object : objects) {
                    try {
                        Log.i(TAG, "try to parse event from LinkedHashMap");
                        events.add(Event.parseFromLinkedHashMap(object));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "error while parsing event, event skipped");
                    }
                    Log.i(TAG, "parsed event from LinkedHashMap successfully");
                }
                Log.i(TAG, "successfully fetched " + String.valueOf(events.size()) + " events");
                callbackHandler.onFetchEventSuccess(events);
            }
        });
    }

    @Override
    public void updateEvent(Event event, String newGuestUserId) {
        Log.i(TAG, "start to update event");

        HashMap<String, String> params = new HashMap<>();
        params.put("eventID",event.getEventId());
        params.put("userID", newGuestUserId);
        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.joinEvent, params, new ResponseHandler());

        Log.i(TAG, "successfully updated event");
    }
}
