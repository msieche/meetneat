package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.UserService;
import com.mannheim.dhbw.wwi13sca.meetneat.fragments.EventOverviewFragment;
import com.mannheim.dhbw.wwi13sca.meetneat.fragments.EventSearchFragment;
import com.mannheim.dhbw.wwi13sca.meetneat.fragments.ProfileFragment;
import com.mannheim.dhbw.wwi13sca.meetneat.fragments.UserEventsFragment;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosLogoutCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.BitmapWorkerTask;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, KumulosLogoutCallbackHandler, EventSearchFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener, UserEventsFragment.OnFragmentInteractionListener, EventOverviewFragment.OnFragmentInteractionListener {
    private static final String TAG = "HomeScreenActivity";

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.navigation_view) NavigationView navigationView;
    @Bind(R.id.drawer_layout) DrawerLayout drawerLayout;

    private MenuItem lastCheckedMenuItem;
    private int currentSelectedFragmentPosition;

    private Bitmap profilePictureBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ButterKnife.bind(this);
        profilePictureBitmap = StringUtils.getBitmapFromString(UserProfile.getInstance().getProfilePicture());

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        navigationView.setNavigationItemSelectedListener(this);
        setupNavigationView();

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        UserService.getInstance().setLogoutCallbackHandler(this);

        if (savedInstanceState != null) {
            currentSelectedFragmentPosition = savedInstanceState.getInt("FRAGMENT_POSITION");
            onNavigationItemSelected(navigationView.getMenu().getItem(currentSelectedFragmentPosition));
            return;
        }
        openDefaultFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("FRAGMENT_POSITION", currentSelectedFragmentPosition);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult called");
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume called");
        int lastFragmentPosition = getPreferences(Activity.MODE_PRIVATE).getInt("FRAGMENT_POSITION", 0);
        onNavigationItemSelected(navigationView.getMenu().getItem(lastFragmentPosition));
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        getPreferences(Activity.MODE_PRIVATE).edit().putInt("FRAGMENT_POSITION", currentSelectedFragmentPosition).commit();
        super.onPause();
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLogoutError(String errorMessage) {

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        if (menuItem == lastCheckedMenuItem) {
            drawerLayout.closeDrawers();
            return false;
        }

        lastCheckedMenuItem = menuItem;

        menuItem.setChecked(!menuItem.isChecked());
        drawerLayout.closeDrawers();

        switch (menuItem.getItemId()) {
            case R.id.event_overview:
                currentSelectedFragmentPosition = 0;
                showFragment(new EventOverviewFragment());
                break;
            case R.id.event_search:
                currentSelectedFragmentPosition = 1;
                showFragment(new EventSearchFragment());
                break;
            case R.id.user_events:
                currentSelectedFragmentPosition = 2;
                showFragment(new UserEventsFragment());
                break;
            case R.id.profile:
                currentSelectedFragmentPosition = 3;
                showFragment(new ProfileFragment());
                break;
            case R.id.logout:
                logoutUser();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    private void setupNavigationView() {
        setupNavigationViewHeader();
        tintNavigationViewMenuIcons(Color.LTGRAY);
    }

    private void tintNavigationViewMenuIcons(int color) {
        MenuItem item = navigationView.getMenu().findItem(R.id.event_overview);
        Drawable icon = item.getIcon();
        icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        item = navigationView.getMenu().findItem(R.id.event_search);
        icon = item.getIcon();
        icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        item = navigationView.getMenu().findItem(R.id.user_events);
        icon = item.getIcon();
        icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        item = navigationView.getMenu().findItem(R.id.profile);
        icon = item.getIcon();
        icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        item = navigationView.getMenu().findItem(R.id.logout);
        icon = item.getIcon();
        icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    private void setupNavigationViewHeader() {
        if (navigationView == null) { return; }

        View navigationViewHeader = navigationView.getHeaderView(0);

        ImageView profilePicture = (ImageView) navigationViewHeader.findViewById(R.id.navigation_header_profile_image);
        TextView username = (TextView) navigationViewHeader.findViewById(R.id.navigation_header_username);
        TextView email = (TextView) navigationViewHeader.findViewById(R.id.navigation_header_email);

        profilePicture.setImageBitmap(profilePictureBitmap);

        username.setText(UserProfile.getInstance().getFirstname() + " " + UserProfile.getInstance().getLastname());
        email.setText(UserProfile.getInstance().getEmail());
    }

    private void openDefaultFragment() {
        navigationView.getMenu().findItem(R.id.event_overview).setChecked(true);
        lastCheckedMenuItem = navigationView.getMenu().findItem(R.id.event_overview);

        currentSelectedFragmentPosition = 0;
        showFragment(new EventOverviewFragment());
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void logoutUser() {
        UserService.getInstance().logoutUser();
    }

    private void loadBitmap(int resId, ImageView imageView) {
        BitmapWorkerTask task = new BitmapWorkerTask(this, imageView);
        task.execute(resId);
    }
}