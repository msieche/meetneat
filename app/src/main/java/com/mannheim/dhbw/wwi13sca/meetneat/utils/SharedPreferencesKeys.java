package com.mannheim.dhbw.wwi13sca.meetneat.utils;

public abstract class SharedPreferencesKeys {
    public static final String PREFERENCES = "Preferences";
    public static final String CURRENT_USER_EMAIL = "email";
    public static final String CURRENT_USER_FIRSTNAME = "current_user_firstname";
    public static final String CURRENT_USER_LASTNAME = "current_user_lastname";
    public static final String CURRENT_USER_CITY = "current_user_city";
    public static final String CURRENT_USER_UID = "current_user_uid";
    public static final String CURRENT_USER_BIRTHYEAR = "current_user_birthyear";
    public static final String CURRENT_USER_GENDER = "current_user_gender";
    public static final String CURRENT_USER_HABITS = "current_user_habits";
    public static final String CURRENT_USER_INFO = "current_user_info";
    public static final String CURRENT_USER_PROFILEPICTURE = "current_user_profilepicture";
    public static final String CURRENT_USER_PHONENUMBER = "current_user_phonenumber";
}