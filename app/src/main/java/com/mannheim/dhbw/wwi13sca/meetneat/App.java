package com.mannheim.dhbw.wwi13sca.meetneat;

import android.app.Application;
import android.content.Context;

import com.kumulos.android.jsonclient.Kumulos;

public class App extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        Kumulos.initWithAPIKeyAndSecretKey("yxqn74b59dd1821nk6dkx58m844cgb5y", "1v171yn6", this);
        Kumulos.setUseSsl(true);
    }
}