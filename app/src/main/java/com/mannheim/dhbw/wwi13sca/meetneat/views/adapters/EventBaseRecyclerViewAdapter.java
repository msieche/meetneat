package com.mannheim.dhbw.wwi13sca.meetneat.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.RecyclerViewButtonClickListener;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.RecyclerViewClickListener;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.DateConverter;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesKeys;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesManager;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventBaseRecyclerViewAdapter extends RecyclerView.Adapter<EventBaseRecyclerViewAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.event_thumbnail) ImageView imageView_thumbnail;
        @Bind(R.id.event_list_title) TextView textView_title;
        @Bind(R.id.event_list_host) TextView textView_host;
        @Bind(R.id.event_list_host_ratingbar) RatingBar ratingBar_host;
        @Bind(R.id.event_list_location) TextView textView_location;
        @Bind(R.id.event_list_datetime) TextView textView_datetime;
        @Bind(R.id.event_list_enterEventButton) Button button_enterEvent;
        // @Bind(R.id.event_list_freePlaces) TextView textView_freePlaces;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            button_enterEvent.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == button_enterEvent.getId()) {
                if (buttonClickListener != null) {
                    buttonClickListener.recyclerViewButtonClicked((Button) view, this.getPosition());
                }
            } else {
                if (itemListener != null) {
                    itemListener.recyclerViewListClicked(view, this.getPosition());
                }
            }
        }

        public void bindEvent(Event event) {
            String imageFile = event.getImageFile();
            if (imageFile != null) {
                imageView_thumbnail.setImageBitmap(StringUtils.getBitmapFromString(imageFile));
            } else {
                Picasso.with(App.context).load(event.getFoodPictureUriString()).placeholder(R.drawable.default_placeholder).error(R.drawable.default_placeholder).into(imageView_thumbnail);
            }
            textView_title.setText(event.getTitle());
            textView_host.setText(event.getHost());
            ratingBar_host.setRating((float) event.getHostRating());
            textView_location.setText(event.getLocation());

            textView_datetime.setText(DateConverter.getLocalizedDateString(
                    DateConverter.getDateFromTimestamp(event.getTimestamp())));

            int freeSpaces = event.getMaxGuests() - event.getGuestIds().size();

            setupEnterButton(event);

            // textView_freePlaces.setText(App.context.getResources().getQuantityString(R.plurals.eventDetail_freePlaces, freeSpaces, freeSpaces));
        }

        private void setupEnterButton(Event event) {
            if (event.getGuestIds().contains(UserProfile.getInstance().getUserId())) {
                button_enterEvent.setText(App.context.getString(R.string.event_state_accepted));
                button_enterEvent.setEnabled(false);
            } else if (event.getMaxGuests() == event.getGuestIds().size()) {
                button_enterEvent.setText(App.context.getString(R.string.event_state_full));
                button_enterEvent.setEnabled(false);
            } else {
                button_enterEvent.setText(App.context.getString(R.string.event_button_text_join_short));
                button_enterEvent.setEnabled(true);
            }
        }
    }

    private List<Event> events = new ArrayList<>();
    private static RecyclerViewClickListener itemListener;
    private static RecyclerViewButtonClickListener buttonClickListener;
    private Context context;

    public EventBaseRecyclerViewAdapter(Context context, RecyclerViewClickListener clickListener, RecyclerViewButtonClickListener buttonClickListener) {
        this.context = context;
        itemListener = clickListener;
        EventBaseRecyclerViewAdapter.buttonClickListener = buttonClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_list_row, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.bindEvent(events.get(position));
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public void clear() {
        events.clear();
        notifyDataSetChanged();
    }

    public void add(Event event) {
        events.add(event);
        notifyDataSetChanged();
    }

    public void sortListByTimestamp() {
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return Long.compare(o1.getTimestamp(), o2.getTimestamp());
            }
        });
    }

    public void enterEventPressed(Activity activity, int eventPosition) {
        String userId = SharedPreferencesManager.getInstance().getString(SharedPreferencesKeys.CURRENT_USER_UID);

        final Event event = events.get(eventPosition);
        event.addGuest(userId);

        notifyItemChanged(eventPosition);

//        Snackbar snackbar = Snackbar
//                .make(activity.findViewById(R.id.viewpager), App.context.getString(R.string.eventList_enterEvent_message, event.getTitle()), Snackbar.LENGTH_LONG)
//                .setAction(R.string.eventList_enterEvent_snackbar_addCalendarEvent, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        startCalendarIntent(event);
//                    }
//                });
//
//        snackbar.show();
    }

    private void startCalendarIntent(Event event) {
        Intent calIntent = new Intent(Intent.ACTION_INSERT);
        calIntent.setType("vnd.android.cursor.item/event");
        calIntent.putExtra(CalendarContract.Events.TITLE, event.getTitle());
        calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, event.getLocation());
        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, event.getDescription());

        calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.getTimestamp());
        calIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(calIntent);
    }
}
