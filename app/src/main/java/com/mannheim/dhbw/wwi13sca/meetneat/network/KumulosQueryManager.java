package com.mannheim.dhbw.wwi13sca.meetneat.network;

import com.kumulos.android.jsonclient.Kumulos;
import com.kumulos.android.jsonclient.ResponseHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.EventService;

import java.util.Map;

public class KumulosQueryManager {
    private static KumulosQueryManager kumulosQueryManager;

    public static KumulosQueryManager getInstance() {
        if (kumulosQueryManager == null) {
            kumulosQueryManager = new KumulosQueryManager();
        }
        return kumulosQueryManager;
    }

    public void executeQuery(String queryName, Map<String, String> params, ResponseHandler responseHandler) {
        Kumulos.call(queryName, params, responseHandler);
    }

    public static String getQuery(EventService.FetchMode fetchMode) {
        switch (fetchMode) {
            case ALL_FUTURE_EVENTS:
                return KumulosQuery.getAllFutureEvents;
            case ALL_HOSTED_EVENTS:
                return KumulosQuery.getAllHostedEventsForUser;
            case ALL_JOINED_EVENTS:
                return KumulosQuery.getAllJoinedEventsForUser;
        }
        return "UNDEFINED";
    }
}
