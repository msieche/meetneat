package com.mannheim.dhbw.wwi13sca.meetneat.Models;

import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesManager;

import java.util.LinkedHashMap;

public class UserProfile {
    private static UserProfile currentUser;

    private String firstname;
    private String lastname;
    private String birthyear;
    private String city;
    private String gender;
    private String zipcode;
    private String email;
    private String phonenumber;
    private double rating;
    private String info;
    private String habits;
    private String profilePicture;
    private String userId;

    private UserProfile() {
    }

    public static UserProfile getInstance() {
        if (currentUser == null) {
            currentUser = new UserProfile();
        }
        return currentUser;
    }

    public static void setCurrentUser(UserProfile user) {
        currentUser = user;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(String birthyear) {
        this.birthyear = birthyear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHabits() {
        return habits;
    }

    public void setHabits(String habits) {
        this.habits = habits;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void createProfile(LinkedHashMap<String,Object> result) {
        setEmail((String) result.get("emailadress"));
        setFirstname((String) result.get("firstname"));
        setLastname((String) result.get("lastname"));
        setCity((String) result.get("city"));
        setGender((String) result.get("gender"));
        setZipcode((String) result.get("zipcode"));
        setPhonenumber((String) result.get("phonenumber"));
        setBirthyear((String) result.get("birthyear"));
        setProfilePicture((String) result.get("profilepicture"));
        setUserId((String) result.get("userID"));
    }
}
