package com.mannheim.dhbw.wwi13sca.meetneat;

import android.util.Log;

import com.kumulos.android.jsonclient.ResponseHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosLoginCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosLogoutCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosLogoutInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosRegistrationCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosRegistrationInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulusLoginInterface;
import com.mannheim.dhbw.wwi13sca.meetneat.network.KumulosQuery;
import com.mannheim.dhbw.wwi13sca.meetneat.network.KumulosQueryManager;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class UserService implements KumulusLoginInterface, KumulosLogoutInterface, KumulosRegistrationInterface {
    private static final String TAG = "UserService";

    private static UserService userService;

    private KumulosLoginCallbackHandler loginCallbackHandler;
    private KumulosLogoutCallbackHandler logoutCallbackHandler;
    private KumulosRegistrationCallbackHandler registrationCallbackHandler;

    private UserService() {}

    public static UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    public void setLoginCallbackHandler(KumulosLoginCallbackHandler loginCallbackHandler) {
        this.loginCallbackHandler = loginCallbackHandler;
    }

    public void setLogoutCallbackHandler(KumulosLogoutCallbackHandler logoutCallbackHandler) {
        this.logoutCallbackHandler = logoutCallbackHandler;
    }

    public void setRegistrationCallbackHandler(KumulosRegistrationCallbackHandler registrationCallbackHandler) {
        this.registrationCallbackHandler = registrationCallbackHandler;
    }

    @Override
    public void logoutUser() {
        Log.i(TAG, "logging out user " + UserProfile.getInstance().getUserId());

        SharedPreferencesManager.getInstance().clearCurrentUser();
        HashMap<String, String> params = new HashMap<>();
        params.put("username ", "testUser");
        params.put("isOnline", "0");
        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.logout, params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                Log.i(TAG, "successfully logged out");
                logoutCallbackHandler.onLogoutSuccess();
            }

            @Override
            public void onFailure(Throwable error) {
                Log.i(TAG, "error during logout");
                logoutCallbackHandler.onLogoutError(error.getMessage());
            }
        });
    }

    @Override
    public void loginWithEmail(final String emailAdress, String password, boolean isOnline) {
        Log.i(TAG, "start login request for emailAdress: " + emailAdress);
        HashMap<String, String> params = new HashMap<>();
        params.put("emailadress", emailAdress);
        params.put("password", password);
        params.put("isOnline", "1");

        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.loginWithEmail, params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                ArrayList<LinkedHashMap<String, Object>> objects = (ArrayList<LinkedHashMap<String,Object>>) result;
                if (objects.size() == 1) {
                    Log.i(TAG, "successfully logged in");
                    UserProfile.getInstance().createProfile(objects.get(0));
                    loginCallbackHandler.onLoginSuccess();
                } else {
                    Log.i(TAG, "error during login");
                    // TODO: add error code or something similar to inform callbackhandler what is happening
                    loginCallbackHandler.onLoginError("");
                }
            }

            @Override
            public void onFailure(Throwable error) {
                Log.i(TAG, "error during login");
                loginCallbackHandler.onLoginError(error.getMessage());
            }
        });
    }

    @Override
    public void createUser(String username, String emailAdress, String password, String firstName, String lastName, String gender, String city, String zipcode, String birthYear, String phoneNumber, String profilePicture, String isOnline) {
        Log.i(TAG, "start to create a new user");
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("emailadress", emailAdress);
        params.put("password", password);
        params.put("firstname", firstName);
        params.put("lastname", lastName);
        params.put("gender", gender);
        params.put("city", city);
        params.put("birthyear", birthYear);
        params.put("phonenumber", phoneNumber);
        params.put("profilepicture", profilePicture);
        params.put("zipcode", zipcode);
        params.put("isOnline", isOnline);
        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.createUser, params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                Log.i(TAG, "created a new user with userId: " + String.valueOf(result));
                UserProfile.getInstance().setUserId(String.valueOf(result));
                registrationCallbackHandler.onRegistrationSuccess();
            }

            @Override
            public void onFailure(Throwable error) {
                registrationCallbackHandler.onRegistrationError(error.getMessage());
            }
        });
    }

    public void createUser(String username, String emailAdress, String password, String firstName, String lastName, String gender, String city, String zipcode, String birthYear, String phoneNumber) {
        createUser(username, emailAdress, password, firstName, lastName, gender, city, zipcode, birthYear, phoneNumber, null, "0");
    }

    public void updateUser(String city, String description) {
        UserProfile.getInstance().setCity(city);
        // TODO: need to implement correct Kumulos API-Method
    }

    public void updateProfilePicture(String imageString) {
        Log.i(TAG, "start to update profile picture");
        UserProfile.getInstance().setProfilePicture(imageString);

        HashMap<String, String> params = new HashMap<>();
        params.put("userID", UserProfile.getInstance().getUserId());
        params.put("profilepicture", imageString);

        KumulosQueryManager.getInstance().executeQuery(KumulosQuery.updateProfilePicture, params, new ResponseHandler());
        Log.i(TAG, "successfully updated profile picture");
    }
}
