package com.mannheim.dhbw.wwi13sca.meetneat.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mannheim.dhbw.wwi13sca.meetneat.EventService;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.views.adapters.EventListRecyclerViewAdapter;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by marce on 12.06.2016.
 */
public class HostedEventsFragment extends Fragment implements EventService.FetchEventCallbackHandler {
    public static final String TAG = "HostedEventsFragment";

    public static class ViewHolder {
        @Bind(R.id.LoadingView) LinearLayout loadingView;
        @Bind(R.id.LoadingView_DotsProgressBar) DilatingDotsProgressBar loadingView_DotsProgressBar;
        @Bind(R.id.recycler_view_event_list) RecyclerView recyclerView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private static ViewHolder viewHolder;
    private EventListRecyclerViewAdapter recyclerViewAdapter;

    public HostedEventsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_list, container, false);

        viewHolder = new ViewHolder(rootView);
        ButterKnife.bind(this, rootView);

        viewHolder.recyclerView.setVisibility(View.GONE);
        viewHolder.loadingView_DotsProgressBar.showNow();

        setupRecyclerView();

        EventService.getInstance().fetchEvents(EventService.FetchMode.ALL_HOSTED_EVENTS, this);

        return rootView;
    }

    private void setupRecyclerView() {
        viewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewAdapter = new EventListRecyclerViewAdapter(getContext(), null, null);
        viewHolder.recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void onFetchEventSuccess(ArrayList<Event> events) {
        recyclerViewAdapter.setNewList(events);

        viewHolder.recyclerView.setVisibility(View.VISIBLE);
        viewHolder.loadingView.setVisibility(View.GONE);
        viewHolder.loadingView_DotsProgressBar.hideNow();

        recyclerViewAdapter.sortListByTimestamp();
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFetchEventError(String errorMessage) {

    }
}
