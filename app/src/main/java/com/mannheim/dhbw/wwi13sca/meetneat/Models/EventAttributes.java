package com.mannheim.dhbw.wwi13sca.meetneat.Models;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.R;

import java.util.Set;

public class EventAttributes {

    private boolean hasVegetarianAttribute;
    private boolean hasVeganAttribute;
    private boolean hasGlutenfreeAttribute;
    private boolean hasWomenOnlyAttribute;
    private boolean hasMenOnlyAttribute;

    public EventAttributes() {
        hasVegetarianAttribute = false;
        hasVeganAttribute = false;
        hasGlutenfreeAttribute = false;
        hasWomenOnlyAttribute = false;
        hasMenOnlyAttribute = false;
    }

    public boolean[] getAttributesArray() {
        return new boolean[]{ hasVegetarianAttribute, hasVeganAttribute, hasGlutenfreeAttribute, hasWomenOnlyAttribute, hasMenOnlyAttribute};
    }

    public void set(Set<String> eventAttributes) {

        for (String attribute : eventAttributes){
            if (attribute.equals(App.context.getResources().getString(R.string.event_detail_vegetarian)))
                hasVegetarianAttribute = true;
            if (attribute.equals(App.context.getResources().getString(R.string.event_detail_vegan)))
                hasVeganAttribute = true;
            if (attribute.equals(App.context.getResources().getString(R.string.event_detail_glutenfree)))
                hasGlutenfreeAttribute = true;
            if (attribute.equals(App.context.getResources().getString(R.string.event_detail_men_only)))
                hasMenOnlyAttribute = true;
            if (attribute.equals(App.context.getResources().getString(R.string.event_detail_women_only)))
                hasWomenOnlyAttribute = true;
        }
    }
}
