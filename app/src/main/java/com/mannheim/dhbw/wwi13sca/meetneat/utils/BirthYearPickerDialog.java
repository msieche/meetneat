package com.mannheim.dhbw.wwi13sca.meetneat.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.BirthYearPickerDialogClickListener;

/**
 * Created by marce on 02.06.2016.
 */
public class BirthYearPickerDialog {
    private BirthYearPickerDialogClickListener birthYearPickerDialogClickListener;
    private AlertDialog.Builder alertDialogBuilder;
    private Context context;
    private NumberPicker numberPicker;

    public BirthYearPickerDialog(Context context, int minValue, int maxValue) {
        this.context = context;
        alertDialogBuilder = new AlertDialog.Builder(this.context, R.style.AppCompatAlertDialogStyle);
        alertDialogBuilder.setTitle(R.string.signup_numberpicker_caption);
        alertDialogBuilder.setView(getNumberPickerDialogAsRelativeLayout(minValue, maxValue, 1990));
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.button_text_ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                birthYearPickerDialogClickListener.onBirthYearPickerDialogValueSelected(numberPicker.getValue());
                            }
                        })
                .setNegativeButton(R.string.button_text_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
    }

    public void show() {
        alertDialogBuilder.show();
        // final AlertDialog alertDialog = alertDialogBuilder.create();
        // alertDialog.show();
    }

    public RelativeLayout getNumberPickerDialogAsRelativeLayout(int minValue, int maxValue, int defaultValue) {
        final RelativeLayout relativeLayout = new RelativeLayout(context);
        numberPicker = new NumberPicker(context);
        numberPicker.setMaxValue(maxValue);
        numberPicker.setMinValue(minValue);

        numberPicker.setValue(defaultValue);

        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPickerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        relativeLayout.setLayoutParams(params);
        relativeLayout.addView(numberPicker, numPickerParams);
        return relativeLayout;

    }

    public void setBirthYearPickerDialogClickListener(BirthYearPickerDialogClickListener birthYearPickerDialogClickListener) {
        this.birthYearPickerDialogClickListener = birthYearPickerDialogClickListener;
    }

    public void setDefaultValue(int value) {
        if (numberPicker == null) { return; }
        numberPicker.setValue(value);
    }
}
