package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

/**
 * Created by marce on 02.06.2016.
 */
public interface KumulosRegistrationInterface {
    void createUser(String username,
                    String emailAdress,
                    String password,
                    String firstName,
                    String lastName,
                    String gender,
                    String city,
                    String zipcode,
                    String birthYear,
                    String phoneNumber,
                    String profilePicture,
                    String isOnline
                    );
}
