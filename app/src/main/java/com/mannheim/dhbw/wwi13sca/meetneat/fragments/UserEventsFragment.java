package com.mannheim.dhbw.wwi13sca.meetneat.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.activities.EventCreateActivity;
import com.mannheim.dhbw.wwi13sca.meetneat.views.adapters.ViewPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserEventsFragment extends Fragment {
    private static final String TAG = "UserEventsFragment";

    public static class ViewHolder {
        @Bind(R.id.user_events_tablayout) TabLayout tabLayout;
        @Bind(R.id.user_events_viewpager) ViewPager viewPager;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private static ViewHolder viewHolder;
    private OnFragmentInteractionListener onFragmentInteractionListener;

    public UserEventsFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    public static UserEventsFragment newInstance() {
        return new UserEventsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_events, container, false);

        getActivity().setTitle(getString(R.string.title_fragment_event_user));

        viewHolder = new ViewHolder(rootView);
        ButterKnife.bind(this, rootView);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(new HostedEventsFragment(), getString(R.string.user_events_title_hosted));
        viewPagerAdapter.addFragment(new JoinedEventsFragment(), getString(R.string.user_events_title_joined));

        viewHolder.viewPager.setAdapter(viewPagerAdapter);
        viewHolder.tabLayout.setupWithViewPager(viewHolder.viewPager);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult called");
        if (requestCode == EventCreateActivity.REQUEST_EVENT_CREATE) {
            if (resultCode == EventCreateActivity.RESULT_EVENT_CREATE_SUCCESS) {
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            onFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentInteractionListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
