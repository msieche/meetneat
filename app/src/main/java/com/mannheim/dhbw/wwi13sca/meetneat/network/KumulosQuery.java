package com.mannheim.dhbw.wwi13sca.meetneat.network;

public abstract class KumulosQuery {
    public static final String getAllFutureJoinedEventsForUser = "getAllFutureJoinedEventsForUser";
    public static final String getAllJoinedEventsForUser = "getAllJoinedEventsForUser";
    public static final String getAllPastJoinedEventsForUser = "getAllPastJoinedEventsForUser";
    public static final String joinEvent = "joinEvent";
    public static final String createEvent = "createEvent";
    public static final String getAllFutureHostedEventsForUser = "getAllFutureHostedEventsForUser";
    public static final String getAllHostedEventsForUser = "getAllHostedEventsForUser";
    public static final String getAllPastEvents = "getAllPastEvents";
    public static final String getAllPastHostedEventsForUser = "getAllPastHostedEventsForUser";
    public static final String createUser = "createUser";
    public static final String getAllUsersByUsername = "getAllUsersByUsername";
    public static final String isUserOnline = "isUserOnline";
    public static final String loginWithEmail = "loginWithEmail";
    public static final String loginWithUsername = "loginWithUsername";
    public static final String logout = "logout";
    public static final String setUserOnlineState = "setUserOnlineState";
    public static final String getAllFutureEvents = "getAllFutureEvents";
    public static final String updateProfilePicture = "updateProfilePicture";
}