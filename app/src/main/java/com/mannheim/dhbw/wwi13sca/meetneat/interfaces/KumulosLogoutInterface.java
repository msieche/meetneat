package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

/**
 * Created by marce on 02.06.2016.
 */
public interface KumulosLogoutInterface {
    void logoutUser();
}
