package com.mannheim.dhbw.wwi13sca.meetneat.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Event implements Parcelable {

    private String title;
    private String host;
    private double hostRating;
    private String location;
    private String description;
    private Bitmap foodPicture_bitmap;
    private String foodPicture_uri_string;
    private int minGuests;
    private int maxGuests;
    private boolean isVeggie;
    private boolean isVegan;
    private boolean isGlutenfree;
    private boolean isWomenOnly;
    private boolean isMenOnly;

    private String state; //should be enum, look here: http://stackoverflow.com/questions/17167144/make-enum-tostring-localized/29625457#29625457
    private String base64ImageFileString;
    private long timestamp;
    private String hostUID;
    private long hostlong;
    private double pricePerPerson;
    private String phoneNumber;

    private List<String> guestIds;
    private String eventId;

    public Event() {
    }

    public Event(String title, String location) {
        this.title = title;
        this.location = location;
    }

    public Event(String title, String host, double hostRating, String location) {
        this.title = title;
        this.host = host;
        this.hostRating = hostRating;
        this.location = location;
    }

    protected Event(Parcel in) {
        title = in.readString();
        host = in.readString();
        hostRating = in.readDouble();
        location = in.readString();
        description = in.readString();
        foodPicture_bitmap = in.readParcelable(null);
        foodPicture_uri_string = in.readString();
        minGuests = in.readInt();
        maxGuests = in.readInt();
        isVeggie = in.readInt() == 1;
        isVegan = in.readInt() == 1;
        isGlutenfree = in.readInt() == 1;
        isMenOnly = in.readInt() == 1;
        isWomenOnly = in.readInt() == 1;
        base64ImageFileString = in.readString();
        timestamp = in.readLong();
        eventId = in.readString();
        state = in.readString();
        pricePerPerson = in.readDouble();
        phoneNumber = in.readString();
        guestIds = new ArrayList<>();
        in.readStringList(guestIds);
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getHostRating() {
        return hostRating;
    }

    public void setHostRating(double hostRating) {
        this.hostRating = hostRating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(host);
        dest.writeDouble(hostRating);
        dest.writeString(location);
        dest.writeString(description);
        dest.writeParcelable(foodPicture_bitmap, flags);
        dest.writeString(foodPicture_uri_string);
        dest.writeInt(minGuests);
        dest.writeInt(maxGuests);
        dest.writeInt(isVeggie ? 1 : 0);
        dest.writeInt(isVegan ? 1 : 0);
        dest.writeInt(isGlutenfree ? 1 : 0);
        dest.writeInt(isMenOnly ? 1 : 0);
        dest.writeInt(isWomenOnly ? 1 : 0);
        dest.writeString(base64ImageFileString);
        dest.writeLong(timestamp);
        dest.writeString(eventId);
        dest.writeString(state);
        dest.writeDouble(pricePerPerson);
        dest.writeString(phoneNumber);
        dest.writeStringList(getGuestIds());
    }

    public Bitmap getFoodPictureBitmap() {
        return foodPicture_bitmap;
    }

    public String getFoodPictureUriString() {
        return foodPicture_uri_string;
    }

    public void setFoodPicture(Bitmap foodPicture, String imageUri_string) {
        this.foodPicture_bitmap = foodPicture;
        this.foodPicture_uri_string = imageUri_string;
    }

    public int getMinGuests() {
        return minGuests;
    }

    public void setMinGuests(int minGuests) {
        this.minGuests = minGuests;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public boolean getIsVeggie() {
        return isVeggie;
    }

    public void setIsVeggie(boolean isVeggie) {
        this.isVeggie = isVeggie;
    }

    public boolean getIsVeganAttribute() {
        return isVegan;
    }

    public void setIsVeganAttribute(boolean isVeganAttribute) {
        this.isVegan = isVeganAttribute;
    }

    public boolean getIsGlutenfree() {
        return isGlutenfree;
    }

    public void setIsGlutenfree(boolean isGlutenfree) {
        this.isGlutenfree = isGlutenfree;
    }

    public boolean getIsWomenOnly() {
        return isWomenOnly;
    }

    public void setIsWomenOnly(boolean isWomenOnly) {
        this.isWomenOnly = isWomenOnly;
    }

    public boolean getIsMenOnly() {
        return isMenOnly;
    }

    public void setIsMenOnly(boolean isMenOnly) {
        this.isMenOnly = isMenOnly;
    }

    public void setImageFile(String base64ImageFileString) {
        this.base64ImageFileString = base64ImageFileString;
    }

    public String getImageFile() {
        return base64ImageFileString;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setHostUID(String hostUID) {
        this.hostUID = hostUID;
    }

    public String getHostUID() {
        return hostUID;
    }

    public long getHostlong() {
        return hostlong;
    }

    public void setHostlong(long hostlong) {
        this.hostlong = hostlong;
    }

    public List<String> getGuestIds() {
        if (guestIds == null) {
            return new ArrayList<>();
        }
        return guestIds;
    }

    public void setGuestIds(ArrayList guestIds) {
        for (int i = 0; i < guestIds.size(); i++) {
            LinkedHashMap<String, Object> guestEntry = (LinkedHashMap<String, Object>) guestIds.get(i);
            if (this.guestIds == null) { this.guestIds = new ArrayList<>(); }
            this.guestIds.add((String) guestEntry.get("userID"));
        }
    }

    public void addGuest(String guestId) {
        if (guestIds == null) {
            guestIds = new ArrayList<>();
        }
        guestIds.add(guestId);
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventId() {
        return eventId;
    }

    public double getPricePerPerson() {
        return pricePerPerson;
    }

    public void setPricePerPerson(double pricePerPerson) {
        this.pricePerPerson = pricePerPerson;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static Event parseFromLinkedHashMap(LinkedHashMap<String, Object> object) {
        Event event = new Event();

        event.setEventId((String) object.get("eventID"));

        event.setTitle((String) object.get("title"));
        event.setTimestamp(Long.valueOf((String) object.get("date"))*1000);
        event.setLocation((String) object.get("location"));
        event.setDescription((String) object.get("description"));
        event.setPricePerPerson(Double.valueOf((String) object.get("price")));
        event.setImageFile((String) object.get("picture"));
        event.setHost((String) object.get("hostName"));
        event.setIsVeggie(object.get("isVeggie").equals("1"));
        event.setIsVeganAttribute(object.get("isVegan").equals("1"));
        event.setIsGlutenfree(object.get("isGlutenfree").equals("1"));
        event.setIsMenOnly(object.get("isMenOnly").equals("1"));
        event.setIsWomenOnly(object.get("isWomenOnly").equals("1"));
        event.setMinGuests(Integer.valueOf((String) object.get("minGuests")));
        event.setMaxGuests(Integer.valueOf((String) object.get("maxGuests")));

        event.setGuestIds((ArrayList) object.get("guests"));

        // TODO: set missing fields: phonenumber, hostRating

        return event;
    }
}
