package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.kumulos.android.jsonclient.Kumulos;
import com.mannheim.dhbw.wwi13sca.meetneat.BuildConfig;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosLoginCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.UserService;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.InputFieldValidator;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesKeys;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesManager;

import CustomDialogs.AuthenticationProgressDialog;
import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements KumulosLoginCallbackHandler {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_email) TextInputEditText emailText;
    @Bind(R.id.input_password) TextInputEditText passwordText;
    @Bind(R.id.btn_login) Button loginButton;
    @Bind(R.id.link_signup) TextView signupLink;

    private AuthenticationProgressDialog authenticationProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        emailText.setText(SharedPreferencesManager.getInstance().getString(SharedPreferencesKeys.CURRENT_USER_EMAIL));

        authenticationProgressDialog = new AuthenticationProgressDialog(this);
        UserService.getInstance().setLoginCallbackHandler(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasValidInputFields()) {
                    loginWithEmail(emailText.getText().toString(), passwordText.getText().toString(), true);
                }
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        if (BuildConfig.DEBUG) {
            loginWithEmail("marcel@gmx.de", "test", true);
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private boolean hasValidInputFields() {
        if (!InputFieldValidator.isValidEmail(emailText.getText().toString())) {
            emailText.setError(getString(R.string.error_invalid_email));
            return false;
        }

        if (passwordText.getText().toString().isEmpty() || passwordText.getText().toString().equals("")) {
            passwordText.setError(getString(R.string.error_password_empty));
            return false;
        }

        return true;
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void loginWithEmail(String emailAdress, String password, boolean isOnline) {
        authenticationProgressDialog.show();
        UserService.getInstance().loginWithEmail(emailAdress, password, isOnline);
    }

    @Override
    public void onLoginSuccess() {
        authenticationProgressDialog.cancel();

        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_EMAIL, emailText.getText().toString());

        Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginError(String errorMessage) {
        authenticationProgressDialog.cancel();
        // TODO: do something with error!
    }
}