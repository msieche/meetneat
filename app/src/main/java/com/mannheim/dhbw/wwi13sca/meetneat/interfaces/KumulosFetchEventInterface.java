package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

import com.mannheim.dhbw.wwi13sca.meetneat.EventService;

/**
 * Created by marce on 09.06.2016.
 */
public interface KumulosFetchEventInterface {
    void fetchEvents(EventService.FetchMode fetchMode, final EventService.FetchEventCallbackHandler callbackHandler);
}
