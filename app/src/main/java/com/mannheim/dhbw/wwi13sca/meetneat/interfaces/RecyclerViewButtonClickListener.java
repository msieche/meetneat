package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

import android.widget.Button;

public interface RecyclerViewButtonClickListener {
    void recyclerViewButtonClicked(Button button, int position);
}
