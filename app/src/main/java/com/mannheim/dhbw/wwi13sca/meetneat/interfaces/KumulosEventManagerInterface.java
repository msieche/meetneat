package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;

/**
 * Created by marce on 11.06.2016.
 */
public interface KumulosEventManagerInterface {
    void updateEvent(Event event, String newGuestId);
}
