package com.mannheim.dhbw.wwi13sca.meetneat.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.EventService;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.activities.EventCreateActivity;
import com.mannheim.dhbw.wwi13sca.meetneat.activities.EventDetailActivity;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.RecyclerViewButtonClickListener;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.RecyclerViewClickListener;
import com.mannheim.dhbw.wwi13sca.meetneat.views.adapters.EndlessRecyclerOnScrollListener;
import com.mannheim.dhbw.wwi13sca.meetneat.views.adapters.EventOverviewListRecyclerViewAdapter;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventOverviewFragment extends Fragment implements RecyclerViewClickListener, RecyclerViewButtonClickListener, SwipeRefreshLayout.OnRefreshListener, EventService.FetchEventCallbackHandler {
    private static final String TAG = "EventOverviewFragment";

    public static final int EVENT_SHOW_DETAILS = 7542;
    private static final int INITIAL_FETCH_SIZE = 10;
    private static final int ITEMS_PER_FETCH = 10;

    private static ViewHolder viewHolder;
    private OnFragmentInteractionListener onFragmentInteractionListener;
    private EventOverviewListRecyclerViewAdapter eventOverviewListAdapter;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    public static class ViewHolder {
        @Bind(R.id.LoadingView) LinearLayout loadingView;
        @Bind(R.id.LoadingView_DotsProgressBar) DilatingDotsProgressBar loadingView_DotsProgressBar;
        @Bind(R.id.eventoverview_list) RecyclerView eventOverviewRecyclerView;
        @Bind(R.id.swipeRefreshContainer) SwipeRefreshLayout swipeRefreshLayout;
        @Bind(R.id.fab_addEvent) FloatingActionButton fabAddEvent;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public EventOverviewFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    public static EventOverviewFragment newInstance() {
        return new EventOverviewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        eventOverviewListAdapter = new EventOverviewListRecyclerViewAdapter(getContext(), this, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_eventoverview, container, false);
        Log.i(TAG, "onCreateView called");

        getActivity().setTitle(getString(R.string.title_fragment_event_overview));

        viewHolder = new ViewHolder(rootView);
        ButterKnife.bind(this, rootView);

        viewHolder.eventOverviewRecyclerView.setVisibility(View.GONE);
        viewHolder.loadingView_DotsProgressBar.showNow();

        setHasOptionsMenu(true);

        setupRecyclerView();

        loadEvents();

        viewHolder.fabAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEventCreateActivity();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EventCreateActivity.REQUEST_EVENT_CREATE) {
            if (resultCode == EventCreateActivity.RESULT_EVENT_CREATE_SUCCESS) {
                eventOverviewListAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == EVENT_SHOW_DETAILS) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.detach(this).attach(this).commit();
        }
    }

    @Override
    public void recyclerViewListClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), EventDetailActivity.class);
        intent.putExtra("SELECTED_EVENT", eventOverviewListAdapter.getEventList().get(position));

        startActivityForResult(intent, EVENT_SHOW_DETAILS);
    }

    @Override
    public void recyclerViewButtonClicked(Button button, int position) {
        eventOverviewListAdapter.enterEventPressed(getActivity(), position);
    }

    @Override
    public void onRefresh() {
        Log.i(TAG, "refreshing items");
        fetchRecentEvents(1, INITIAL_FETCH_SIZE);
        endlessRecyclerOnScrollListener.reset(0, false);
    }

    @Override
    public void onFetchEventSuccess(ArrayList<Event> events) {
        eventOverviewListAdapter.setNewList(events);

        hideLoadingView();
        showEventList();

        // Animation animation = AnimationUtils.loadAnimation(App.context, R.anim.simple_grow);
        //viewHolder.fabAddEvent.startAnimation(animation);

        eventOverviewListAdapter.sortListByTimestamp();
        eventOverviewListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFetchEventError(String errorMessage) {
        Log.e(TAG, errorMessage);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        viewHolder.eventOverviewRecyclerView.setLayoutManager(linearLayoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.i(TAG, "load " + String.valueOf(current_page * ITEMS_PER_FETCH) + " items");
                fetchRecentEvents(current_page, ITEMS_PER_FETCH);
            }
        };

        viewHolder.eventOverviewRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);

        viewHolder.swipeRefreshLayout.setOnRefreshListener(this);
        viewHolder.eventOverviewRecyclerView.setAdapter(eventOverviewListAdapter);
    }

    private void fetchRecentEvents(int currentPage, int eventsToFetch) {
        Log.i(TAG, "fetching items...");
        EventService.getInstance().fetchEvents(EventService.FetchMode.ALL_FUTURE_EVENTS, this);
    }

    private void startEventCreateActivity() {
        Intent intent = new Intent(getActivity(), EventCreateActivity.class);
        startActivityForResult(intent, EventCreateActivity.REQUEST_EVENT_CREATE);
    }

    private void loadEvents() {
        if (eventOverviewListAdapter.shouldUpdateEventList()) {
            fetchRecentEvents(1, INITIAL_FETCH_SIZE);
        } else {
            hideLoadingView();
            showEventList();
        }
    }

    private void hideLoadingView() {
        if (viewHolder.swipeRefreshLayout.isRefreshing()) {
            viewHolder.swipeRefreshLayout.setRefreshing(false);
        }

        viewHolder.loadingView.setVisibility(View.GONE);
        viewHolder.loadingView_DotsProgressBar.hideNow();
    }

    private void showEventList() {
        viewHolder.eventOverviewRecyclerView.setVisibility(View.VISIBLE);
        viewHolder.fabAddEvent.show();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            onFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentInteractionListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
