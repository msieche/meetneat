package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.UserService;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.InputFieldValidator;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditProfileActivity extends AppCompatActivity {

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.profile_userPicture) ImageView userPicture;
    @Bind(R.id.profile_name_firstname) EditText inputFirstname;
    @Bind(R.id.profile_name_lastname) EditText inputLastname;
    @Bind(R.id.profile_location_zipcode) EditText inputZipcode;
    @Bind(R.id.profile_location_city) EditText inputCity;
    @Bind(R.id.profile_phone) EditText inputPhone;
    @Bind(R.id.profile_email) EditText inputEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prepopulateFieldsFromUserprofile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.menu_profile_edit, menu);
        menu.findItem(R.id.action_save).setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            // trigger kumulos update, update local UserProfile class
            // shared element transitions
            if (fieldsHaveValidInput()) {
                Toast.makeText(this, "Saved changes", Toast.LENGTH_LONG).show();
                saveChanges();
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepopulateFieldsFromUserprofile() {
        inputFirstname.setText(UserProfile.getInstance().getFirstname());
        inputLastname.setText(UserProfile.getInstance().getLastname());
        inputZipcode.setText(UserProfile.getInstance().getZipcode());
        inputCity.setText(UserProfile.getInstance().getCity());
        inputPhone.setText(UserProfile.getInstance().getPhonenumber());
        inputEmail.setText(UserProfile.getInstance().getEmail());
    }

    private boolean fieldsHaveValidInput() {

        if (!InputFieldValidator.isValidName(inputFirstname.getText().toString())) {
            inputFirstname.setError("");
            return false;
        }

        if (!InputFieldValidator.isValidName(inputLastname.getText().toString())) {
            inputLastname.setError("");
            return false;
        }

        if (!InputFieldValidator.isValidZipcode(inputZipcode.getText().toString())) {
            inputZipcode.setError(getString(R.string.error_invalid_zipcode));
            return false;
        }

        if (!InputFieldValidator.isValidCity(inputCity.getText().toString())) {
            inputCity.setError(getString(R.string.error_invalid_eventLocation));
            return false;
        }

        if (!InputFieldValidator.isValidPhoneNumber(inputPhone.getText().toString())) {
            inputPhone.setError(getString(R.string.error_invalid_phonenumber));
            return false;
        }

        if (!InputFieldValidator.isValidEmail(inputEmail.getText().toString())) {
            inputEmail.setError(getString(R.string.error_invalid_email));
            return false;
        }

        return true;
    }

    private void saveChanges() {
        // UserService.getInstance().updateUser();
    }
}
