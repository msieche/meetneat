package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.mannheim.dhbw.wwi13sca.meetneat.EventService;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.AddPhotoDialog;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.CurrencyFormatInputFilter;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.DateConverter;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.InputFieldValidator;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.StringUtils;

import org.droidparts.widget.ClearableEditText;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventCreateActivity extends AppCompatActivity implements EventService.CreateEventCallbackHandler {
    private final static String TAG = "EventCreateActivity";

    public final static int REQUEST_EVENT_CREATE = 6178;
    public final static int RESULT_EVENT_CREATE_SUCCESS = 6179;

    private String imageFile;
    private boolean[] eventAttrs;
    private long timestamp;

    @Bind(R.id.toolbar) Toolbar toolbar;

    @Bind(R.id.input_title) ClearableEditText editText_eventTitle;
    @Bind(R.id.input_datetime) EditText editText_eventDateTime;
    @Bind(R.id.input_location) ClearableEditText editText_eventLocation;
    @Bind(R.id.input_description) EditText editText_eventDescription;
    @Bind(R.id.input_minGuests) EditText editText_minGuests;
    @Bind(R.id.input_maxGuests) EditText editText_maxGuests;
    @Bind(R.id.input_pricePerPerson) EditText editText_eventPricePerPerson;
    @Bind(R.id.button_createEvent) Button button_createEvent;
    @Bind(R.id.button_selectPhoto) Button button_selectPhoto;
    @Bind(R.id.button_setEventDetails) Button button_addFurtherDetails;
    @Bind(R.id.ivImage) ImageView imageView_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_create);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventService.getInstance().setCreateEventCallbackHandler(this);

        setUpGuiElements();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                showCancelDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Log.i(TAG, "request cancel creation...");
        showCancelDialog();
    }

    private void showCancelDialog() {
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setMessage(getResources().getString(R.string.event_create_message_onbackpressed))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.event_create_button_text_cancel_positive), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(TAG, "cancel creation of event");
                        EventCreateActivity.this.finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.event_create_button_text_cancel_negative), null)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult called");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AddPhotoDialog.REQUEST_CAMERA) {
                Log.i(TAG, "process captured photo");
                Uri picUri = data.getData();
                performCrop(picUri);

                // should we save the image on the device?????
                /*
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Log.i(TAG, "Thumbnail width: " + thumbnail.getWidth());
                Log.i(TAG, "Thumbnail height: " + thumbnail.getHeight());
                imageView_photo.setImageBitmap(thumbnail);
                imageFile = StringUtils.getStringFromBitmap(thumbnail, 100);
                */
            } else if (requestCode == AddPhotoDialog.SELECT_FILE) {
                Log.i(TAG, "process selected photo");
            }
            else if (requestCode == 55) {
                //get the returned data
                Bundle extras = data.getExtras();
                //get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                Log.i(TAG, "picture was cropped");
                Log.i(TAG, "Bitmap Height: " + thePic.getHeight());
                Log.i(TAG, "Bitmap Width: " + thePic.getWidth());
                imageView_photo.setImageBitmap(thePic);
                imageFile = StringUtils.getStringFromBitmap(thePic, 100);
                Log.i(TAG, "STOP");
            }
        }
    }

    private void performCrop(Uri picUri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);

            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, 55);

        } catch(Exception exception) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void setUpGuiElements() {
        setUpButtons();
        setUpEditTexts();
    }

    private void setUpButtons() {
        setUpCreateEventButton();
        setUpSelectPhotoButton();
        setUpAddFurtherDetailsButton();
    }

    private void setUpAddFurtherDetailsButton() {

        button_addFurtherDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EventCreateActivity.this, R.style.AppCompatAlertDialogStyle);
                final String[] items = getResources().getStringArray(R.array.array_event_details_options);
                if (eventAttrs == null) {
                    eventAttrs = new boolean[]{
                            false, // veggie
                            false, // vegan
                            false, // gluten
                            false, // only men
                            false // only women
                    };
                }
                builder.setMultiChoiceItems(R.array.array_event_details_options, eventAttrs, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // avoid only men AND only women
                        final AlertDialog alert = (AlertDialog) dialog;
                        final ListView list = alert.getListView();
                        eventAttrs[which] = isChecked;
                        if (which == 3) {
                            list.setItemChecked(4, false);
                            eventAttrs[4] = false;
                        }
                        if (which == 4) {
                            list.setItemChecked(3, false);
                            eventAttrs[3] = false;
                        }
                    }
                });
                builder.setCancelable(false);
                builder.setTitle(getString(R.string.event_create_details_caption));
                builder.setPositiveButton(getString(R.string.button_text_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.setNegativeButton(getString(R.string.button_text_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void setUpSelectPhotoButton() {
        button_selectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddPhotoDialog();
            }
        });
    }

    private void showAddPhotoDialog() {
        AddPhotoDialog addPhotoDialog = new AddPhotoDialog(EventCreateActivity.this) {
            @Override
            public void onTakePhotoClicked() {
                startActivityForResult(getCapturePhotoIntent(80, 80), REQUEST_CAMERA);
            }

            @Override
            public void onSelectPhotoClicked() {
                Toast.makeText(getApplicationContext(), "Selecting Photo from Gallery is not supported yet", Toast.LENGTH_SHORT).show();
                //startActivityForResult(getSelectPhotoIntent(), SELECT_FILE);
            }
        };
        addPhotoDialog.show();
    }

    private void setUpCreateEventButton() {
        button_createEvent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validateInputFields()) {

                    String firstName = UserProfile.getInstance().getFirstname();
                    String lastName = UserProfile.getInstance().getLastname();
                    String host = firstName + " " + lastName.charAt(0) + ".";

                    if (eventAttrs == null) {
                        eventAttrs = new boolean[]{
                                false, // veggie
                                false, // vegan
                                false, // gluten
                                false, // only men
                                false // only women
                        };
                    }

                    EventService.getInstance().createEvent(
                            editText_eventTitle.getText().toString(),
                            String.valueOf(timestamp),
                            editText_eventLocation.getText().toString(),
                            editText_eventDescription.getText().toString(),
                            editText_eventPricePerPerson.getText().toString(),
                            imageFile,
                            host,
                            eventAttrs,
                            editText_minGuests.getText().toString(),
                            editText_maxGuests.getText().toString()
                    );
                }
            }
        });
    }

    private boolean validateInputFields() {
        if (!InputFieldValidator.isValidEventTitle(editText_eventTitle.getText().toString())) {
            editText_eventTitle.setError(getString(R.string.error_invalid_eventTitle));
            return false;
        }

        if (!InputFieldValidator.isValidCity(editText_eventLocation.getText().toString())) {
            editText_eventLocation.setError(getString(R.string.error_invalid_eventLocation));
            return false;
        }

        if (!InputFieldValidator.isValidMinGuestAmount(editText_minGuests.getText().toString())) {
            editText_minGuests.setError(getString(R.string.error_invalid_minGuests));
            return false;
        }

        if (!InputFieldValidator.isValidMaxGuestAmount(editText_maxGuests.getText().toString())) {
            editText_maxGuests.setError(getString(R.string.error_invalid_maxGuests));
            return false;
        }

        if (!InputFieldValidator.isValidEventGuestRange(editText_minGuests.getText().toString(), editText_maxGuests.getText().toString())) {
            editText_maxGuests.setError("MaxGuests have to be higher than MinGuests");
            return false;
        }

        if (!InputFieldValidator.isValidPrice(editText_eventPricePerPerson.getText().toString())) {
            editText_eventPricePerPerson.setError(getString(R.string.error_invalid_price));
            return false;
        }

        return true;
    }

    private void setUpEditTexts() {
        editText_eventPricePerPerson.setFilters(new InputFilter[] { new CurrencyFormatInputFilter()});
        setUpEventDateTimeEditText();
    }

    private void setUpEventDateTimeEditText() {

        // final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getDateTimeInstance());
        final SlideDateTimeListener listener = new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date) {
                editText_eventDateTime.setText(DateConverter.getLocalizedDateString(date));
                timestamp = DateConverter.getUnixTimestamp(date);
            }
        };


        editText_eventDateTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    new SlideDateTimePicker.Builder(getSupportFragmentManager())
                            .setListener(listener)
                            .setInitialDate(new Date())
                            .setMinDate(new Date())
                            .setIndicatorColor(getResources().getColor(R.color.meetneat_green))
                                    //.setMaxDate(maxDate)
                                    //.setIs24HourTime(true)
                                    //.setTheme(SlideDateTimePicker.HOLO_DARK)
                            .build()
                            .show();
                }
                return false;
            }
        });

        editText_eventDateTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new SlideDateTimePicker.Builder(getSupportFragmentManager())
                            .setListener(listener)
                            .setInitialDate(new Date())
                            .setMinDate(new Date())
                            .setIndicatorColor(getResources().getColor(R.color.meetneat_green))
                            .build()
                            .show();
                    //.setMaxDate(maxDate)
                    //.setIs24HourTime(true)
                    //.setTheme(SlideDateTimePicker.HOLO_DARK)
                }
            }
        });
    }

    @Override
    public void onCreateEventSuccess() {
        setResult(RESULT_EVENT_CREATE_SUCCESS);
        finish();
    }

    @Override
    public void onCreateEventError(String errorMessage) {

    }
}
