package com.mannheim.dhbw.wwi13sca.meetneat.interfaces;

/**
 * Created by marce on 02.06.2016.
 */
public interface KumulosCreateEventInterface {
    void createEvent(String title,
                    String date,
                    String location,
                    String description,
                    String isVeggie,
                    String isVegan,
                    String isGlutenfree,
                    String isMenOnly,
                    String isWomenOnly,
                    String price,
                    String picture,
                    String hostName,
                    String minGuests,
                    String maxGuests
    );
}
