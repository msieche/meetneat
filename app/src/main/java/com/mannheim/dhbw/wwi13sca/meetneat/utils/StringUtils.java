package com.mannheim.dhbw.wwi13sca.meetneat.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class StringUtils {

    public static String[] convertToStringArray(CharSequence[] charSequences) {
        if (charSequences instanceof String[]) {
            return (String[]) charSequences;
        }

        String[] strings = new String[charSequences.length];
        for (int index = 0; index < charSequences.length; index++) {
            strings[index] = charSequences[index].toString();
        }

        return strings;
    }

    public static String removeSpecialCharacters(String input) {
        return input.replaceAll("[^0-9]", "");
    }

    public static String getStringFromBitmap(Bitmap bitmap, int quality) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, quality, bytes);
        byte[] byteArray = bytes.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap getBitmapFromString(String string) {
        byte[] imageAsBytes = Base64.decode(string, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

}
