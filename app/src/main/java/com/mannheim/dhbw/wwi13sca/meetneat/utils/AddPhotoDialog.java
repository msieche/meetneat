package com.mannheim.dhbw.wwi13sca.meetneat.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.mannheim.dhbw.wwi13sca.meetneat.R;

public abstract class AddPhotoDialog {
    private static final String TAG = "AddPhotoDialog";

    public final static int REQUEST_CAMERA = 6180;
    public final static int SELECT_FILE = 6181;

    private AlertDialog.Builder builder;
    private Context context;
    private final CharSequence[] items;

    private final DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int item) {
            if (items[item].equals(context.getString(R.string.dialog_photo_action_takephoto))) {
                onTakePhotoClicked();
            } else if (items[item].equals(context.getString(R.string.dialog_photo_action_selectphoto))) {
                onSelectPhotoClicked();
            }
        }
    };

    public AddPhotoDialog(final Context context) {
        this.context = context;
        builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);

        items = context.getResources().getStringArray(R.array.array_dialog_options_photo_add);
        builder.setItems(items, onClickListener);

        String title = context.getString(R.string.dialog_title_photo_add);
        builder.setTitle(title);

        builder.setNegativeButton(context.getString(R.string.button_text_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
    }

    public void show() {
        builder.show();
    }

    public Intent getCapturePhotoIntent(int outputXDp, int outputYDp) {
        Log.i(TAG, "requesting CapturePhotoIntent...");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // intent.putExtra("outputX", Utils.convertDpToPixel(outputXDp, context.getApplicationContext()));
        // intent.putExtra("outputY", Utils.convertDpToPixel(outputYDp, context.getApplicationContext()));
        return intent;
    }

    public Intent getSelectPhotoIntent() {
        Log.i(TAG, "requesting SelectPhotoIntent...");
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        return intent;
    }

    public abstract void onTakePhotoClicked();
    public abstract void onSelectPhotoClicked();
}
