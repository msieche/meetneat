package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.UserService;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.BirthYearPickerDialogClickListener;
import com.mannheim.dhbw.wwi13sca.meetneat.interfaces.KumulosRegistrationCallbackHandler;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.BirthYearPickerDialog;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.InputFieldValidator;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesKeys;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.SharedPreferencesManager;

import CustomDialogs.AuthenticationProgressDialog;
import butterknife.Bind;
import butterknife.ButterKnife;
import co.geeksters.googleplaceautocomplete.lib.CustomAutoCompleteTextView;

public class SignupActivity extends AppCompatActivity implements KumulosRegistrationCallbackHandler, BirthYearPickerDialogClickListener {
    private static final String TAG = "SignupActivity";

    @Bind(R.id.input_lastname) TextInputEditText lastnameText;
    @Bind(R.id.input_firstname) TextInputEditText firstnameText;
    @Bind(R.id.input_email) TextInputEditText emailText;
    @Bind(R.id.input_phoneNumber) TextInputEditText phonenumberText;
    @Bind(R.id.input_password) TextInputEditText passwordText;
    @Bind(R.id.input_password_confirm) TextInputEditText passwordconfirm_Text;
    @Bind(R.id.btn_signup) Button signupButton;
    @Bind(R.id.link_login) TextView loginLink;
    @Bind(R.id.profile_input_year) EditText yearText;
    @Bind(R.id.radioGroupGender) RadioGroup radioGroupGender;
    @Bind(R.id.radioButton_male) RadioButton radioButtonMale;
    @Bind(R.id.input_city) TextInputEditText cityText;
    @Bind(R.id.input_zipcode) TextInputEditText zipcodeText;

    private AuthenticationProgressDialog authenticationProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        authenticationProgressDialog = new AuthenticationProgressDialog(this, getString(R.string.signup_message));

        UserService.getInstance().setRegistrationCallbackHandler(this);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        yearText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showBirthYearPickerDialog();
                }
                return false;
            }

        });

        phonenumberText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phonenumberText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        lastnameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        firstnameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButtonMale.setError(null);
            }
        });

    }

    private void showBirthYearPickerDialog() {
        yearText.setError(null);
        BirthYearPickerDialog birthYearPickerDialog = new BirthYearPickerDialog(SignupActivity.this, 1926, 2012) {
        };
        birthYearPickerDialog.setDefaultValue(yearText.getText().toString().equals("") ? 1990 : Integer.valueOf(yearText.getText().toString()));
        birthYearPickerDialog.setBirthYearPickerDialogClickListener(this);
        birthYearPickerDialog.show();
    }

    public void signup() {
        if (hasValidInputFields()) {
            authenticationProgressDialog.show();
            signupButton.setEnabled(false);

            UserService.getInstance().createUser(
                    "userName",
                    emailText.getText().toString(),
                    passwordText.getText().toString(),
                    firstnameText.getText().toString(),
                    lastnameText.getText().toString(),
                    radioButtonMale.isChecked() ? "male" : "female",
                    cityText.getText().toString(),
                    zipcodeText.getText().toString(),
                    yearText.getText().toString(),
                    phonenumberText.getText().toString());
        }
    }

    private boolean hasValidInputFields() {
        if (!InputFieldValidator.isValidEmail(emailText.getText().toString())) {
            emailText.setError(getString(R.string.error_invalid_email));
            return false;
        }

        if (!InputFieldValidator.isValidPhoneNumber(phonenumberText.getText().toString())) {
            phonenumberText.setError(getString(R.string.error_invalid_phonenumber));
            return false;
        }

        if (!InputFieldValidator.isValidPassword(passwordText.getText().toString())) {
            passwordText.setError(getString(R.string.error_invalid_password));
            return false;
        }

        if (!passwordText.getText().toString().equals(passwordconfirm_Text.getText().toString())) {
            passwordconfirm_Text.setError(getString(R.string.error_passwords_not_identical));
            return false;
        }

        if (firstnameText.getText().toString().isEmpty() || firstnameText.getText().toString().equals("")) {
            firstnameText.setError(getString(R.string.error_signup_enterFirstname));
            return false;
        }

        if (lastnameText.getText().toString().isEmpty() || lastnameText.getText().toString().equals("")) {
            lastnameText.setError(getString(R.string.error_signup_enterLastname));
            return false;
        }

        if (yearText.getText().toString().isEmpty() || yearText.getText().toString().equals("")) {
            yearText.setError(getString(R.string.error_signup_birthyear));
            return false;
        }

        if (radioGroupGender.getCheckedRadioButtonId() == -1) {
            radioButtonMale.setError(getString(R.string.error_signup_gender));
            return false;
        }

        if (cityText.getText().toString().isEmpty() || cityText.getText().toString().equals("")) {
            cityText.setError(getString(R.string.error_signup_city));
            return false;
        }

        if (!InputFieldValidator.isValidZipcode(zipcodeText.getText().toString())) {
            zipcodeText.setError(getString(R.string.error_invalid_zipcode));
            return false;
        }

        return true;
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onRegistrationSuccess() {
        authenticationProgressDialog.cancel();
        signupButton.setEnabled(true);

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        SharedPreferencesManager.getInstance().addString(SharedPreferencesKeys.CURRENT_USER_EMAIL, emailText.getText().toString());

        startActivity(intent);
        finish();
    }

    @Override
    public void onRegistrationError(String errorMessage) {
        authenticationProgressDialog.cancel();
        signupButton.setEnabled(true);
    }

    @Override
    public void onBirthYearPickerDialogValueSelected(int value) {
        yearText.setText(String.valueOf(value));
    }
}