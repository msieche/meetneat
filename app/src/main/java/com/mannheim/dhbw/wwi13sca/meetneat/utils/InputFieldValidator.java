package com.mannheim.dhbw.wwi13sca.meetneat.utils;

public class InputFieldValidator {

    public static boolean isValidEmail(String email) {
        return email.equals("a") || !(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    public static boolean isValidPassword(String password) {
        return !(password.isEmpty() || password.length() < 4 || password.length() > 10);
    }

    public static boolean isValidEventTitle(String eventTitle) {
        return !(eventTitle.isEmpty() || eventTitle.length() < 4 || eventTitle.length() > 20);
    }

    public static boolean isValidCity(String city) {
        return !city.isEmpty();
    }

    public static boolean isValidMinGuestAmount(String minGuests) {
        return !(minGuests.isEmpty() || Integer.parseInt(minGuests) < 1);
    }

    public static boolean isValidMaxGuestAmount(String maxGuests) {
        return !(maxGuests.isEmpty() || Integer.parseInt(maxGuests) < 1);
    }

    public static boolean isValidEventGuestRange(String minGuests, String maxGuests) {
        return Integer.parseInt(maxGuests) > Integer.parseInt(minGuests);
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.length() >= 7 && phoneNumber.length() <= 13;
    }

    public static boolean isValidPrice(String pricePerPerson) {
        NumberFormatException e = null;
        try {
            double price = Double.valueOf(pricePerPerson);
        } catch (NumberFormatException nfe) {
            e = nfe;
        }
        return e == null;
    }

    public static boolean isValidName(String name) {
        return !name.isEmpty();
    }

    public static boolean isValidZipcode(String zipcode) {
        return zipcode.length() == 5;
    }
}