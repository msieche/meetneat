package com.mannheim.dhbw.wwi13sca.meetneat.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.EventService;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.Event;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.DateConverter;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.ProfileDialog;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.StringUtils;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

// TODO: add functionality to see profile of host (e.g. in a fragment)

public class EventDetailActivity extends AppCompatActivity {
    private final static String TAG = "EventDetailActivity";

    @Bind(R.id.event_detail_host) LinearLayout hostFrame;
    @Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsing_toolbar;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.eventDetail_foodPicture) ImageView imageView_foodPicture;
    @Bind(R.id.eventDetail_host) TextView textView_host;
    @Bind(R.id.eventDetail_phoneNumber) TextView textView_phoneNumber;
    @Bind(R.id.eventDetail_host_ratingbar) RatingBar ratingBar_host;
    @Bind(R.id.eventDetail_location) TextView textView_location;
    @Bind(R.id.eventDetail_date) TextView textView_date;
    @Bind(R.id.eventDetail_guests) TextView textView_guests;
    @Bind(R.id.eventDetail_price) TextView textView_price;
    @Bind(R.id.eventDetail_details_text) EditText editText_details;
    @Bind(R.id.eventDetail_details_vegetarian) CheckBox checkBox_vegetarian;
    @Bind(R.id.eventDetail_details_vegan) CheckBox checkBox_vegan;
    @Bind(R.id.eventDetail_details_glutenfree) CheckBox checkBox_glutenfree;
    @Bind(R.id.eventDetail_details_menOnly) CheckBox checkBox_menOnly;
    @Bind(R.id.eventDetail_details_womenOnly) CheckBox checkBox_womenOnly;
    @Bind(R.id.eventDetail_enterEventButton) Button button_enterEvent;

    private Event event;

    private final View.OnTouchListener imageViewTouchListener =new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
//            Dialog builder = new Dialog(EventDetailActivity.this);
//            builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialogInterface) {
//                    //nothing;
//                }
//            });
//            ImageView originImageView = (ImageView) v;
//            ImageView imageView = new ImageView(EventDetailActivity.this);
//            imageView.setImageBitmap(((BitmapDrawable)originImageView.getDrawable()).getBitmap());
//            //below code fullfil the requirement of xml layout file for dialoge popup
//            builder.addContentView(imageView, new RelativeLayout.LayoutParams(
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT));
//            builder.show();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "received event");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        event = getIntent().getParcelableExtra("SELECTED_EVENT");
        collapsing_toolbar.setTitle(event.getTitle());
        setUpActivity();

        hostFrame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showHostProfileDialogFragment();
            }
        });



        imageView_foodPicture.setOnTouchListener(imageViewTouchListener);
        button_enterEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterEvent();
            }
        });
    }

    private void showHostProfileDialogFragment() {

        FragmentManager fm = getSupportFragmentManager();
        ProfileDialog editNameDialogFragment = ProfileDialog.newInstance(
                textView_host.getText().toString(),
                null,
                ratingBar_host.getRating(),
                textView_phoneNumber.getText().toString()
                );
        editNameDialogFragment.show(fm, "fragment_edit_name");


    }

    private void enterEvent() {
        String userId = UserProfile.getInstance().getUserId();

        event.addGuest(userId);

        EventService.getInstance().updateEvent(event, userId);

        Toast.makeText(getApplicationContext(), getString(R.string.event_list_message_event_enter, event.getTitle()), Toast.LENGTH_SHORT).show();
        button_enterEvent.setText(getApplicationContext().getString(R.string.event_state_accepted_long));
        button_enterEvent.setEnabled(false);
    }

    private void setUpActivity() {
        setTitle(event.getTitle());

        String imageFile = event.getImageFile();
        if (imageFile != null) {
            imageView_foodPicture.setImageBitmap(StringUtils.getBitmapFromString(imageFile));
        } else {
            Picasso.with(this).load(event.getFoodPictureUriString()).placeholder(R.drawable.default_placeholder).fit().into(imageView_foodPicture);
        }

        fillEventDetailsHeader();
        setupFurtherDetails();
        setupEnterButton();
    }

    private void setupEnterButton() {
        // TODO: check for event date (if its in the past you mustnt be able to join
        if (event.getGuestIds().contains(UserProfile.getInstance().getUserId())) {
            button_enterEvent.setText(App.context.getString(R.string.event_state_accepted));
            button_enterEvent.setEnabled(false);
        } else if (event.getMaxGuests() == event.getGuestIds().size()) {
            button_enterEvent.setText(App.context.getString(R.string.event_state_full));
            button_enterEvent.setEnabled(false);
        } else {
            button_enterEvent.setText(App.context.getString(R.string.event_button_text_join_short));
            button_enterEvent.setEnabled(true);
        }
    }

    private void fillEventDetailsHeader() {
        textView_host.setText(event.getHost());
        textView_phoneNumber.setText(event.getPhoneNumber());
        textView_location.setText(event.getLocation());
        textView_date.setText(DateConverter.getLocalizedDateString(
                DateConverter.getDateFromTimestamp(event.getTimestamp())));
        textView_guests.setText(String.format(getResources().getString(R.string.event_detail_text_guests), event.getMinGuests(), event.getMaxGuests()));
        ratingBar_host.setRating((float) event.getHostRating());
        textView_price.setText(getString(R.string.event_detail_text_price, event.getPricePerPerson()));
    }

    private void setupFurtherDetails() {
        editText_details.setText(event.getDescription());
        setupCheckboxes();
    }

    private void setupCheckboxes() {
        checkCheckboxes();
        disableCheckboxes();
    }

    private void checkCheckboxes() {
        checkBox_vegetarian.setChecked(event.getIsVeggie());
        checkBox_vegan.setChecked(event.getIsVeganAttribute());
        checkBox_glutenfree.setChecked(event.getIsGlutenfree());
        checkBox_menOnly.setChecked(event.getIsMenOnly());
        checkBox_womenOnly.setChecked(event.getIsWomenOnly());
    }

    private void disableCheckboxes() {
        checkBox_vegetarian.setEnabled(false);
        checkBox_vegan.setEnabled(false);
        checkBox_glutenfree.setEnabled(false);
        checkBox_menOnly.setEnabled(false);
        checkBox_womenOnly.setEnabled(false);
    }
}
