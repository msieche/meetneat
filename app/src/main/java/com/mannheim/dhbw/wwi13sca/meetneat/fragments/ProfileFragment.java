package com.mannheim.dhbw.wwi13sca.meetneat.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mannheim.dhbw.wwi13sca.meetneat.App;
import com.mannheim.dhbw.wwi13sca.meetneat.Models.UserProfile;
import com.mannheim.dhbw.wwi13sca.meetneat.R;
import com.mannheim.dhbw.wwi13sca.meetneat.UserService;
import com.mannheim.dhbw.wwi13sca.meetneat.activities.EditProfileActivity;
import com.mannheim.dhbw.wwi13sca.meetneat.utils.StringUtils;

import java.io.File;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

// TODO: improve edit mode layout

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    public static class ViewHolder {

        @Bind(R.id.profile_userPicture) ImageView imageView_profilePicture;
        @Bind(R.id.profile_username) TextView textView_username;
        @Bind(R.id.profile_phonenumber) TextView textView_phonenumber;
        @Bind(R.id.profile_userCity) TextView textView_usercity;
        @Bind(R.id.profile_email) TextView textView_userEmail;
        @Bind(R.id.profile_userRatingbar) RatingBar ratingBar_userRating;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    private static ViewHolder viewHolder;

    private OnFragmentInteractionListener onFragmentInteractionListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        viewHolder = new ViewHolder(rootView);

        getActivity().setTitle(getString(R.string.title_fragment_profile));
        loadUserProfile();

        viewHolder.imageView_profilePicture.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(ProfileFragment.this, "Pick Photo", 0);
            }
        });

        imitateElevationForPreLollipopDevices();

        setHasOptionsMenu(true);

        return rootView;
    }

    private void imitateElevationForPreLollipopDevices() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            startActivity(new Intent(getContext(), EditProfileActivity.class));
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);
    }

    private void loadUserProfile() {
        Log.i(TAG, "load user profile");
        String phoneNumber = UserProfile.getInstance().getPhonenumber();
        String firstName = UserProfile.getInstance().getFirstname();
        String lastName = UserProfile.getInstance().getLastname();
        String city = UserProfile.getInstance().getCity();
        String zipcode = UserProfile.getInstance().getZipcode();
        String birthyear = UserProfile.getInstance().getBirthyear();
        String email = UserProfile.getInstance().getEmail();

        String profilePicture = UserProfile.getInstance().getProfilePicture();;

        if (profilePicture != null) {
            Bitmap bitmap = getCircleBitmap(StringUtils.getBitmapFromString(profilePicture));
            viewHolder.imageView_profilePicture.setImageBitmap(bitmap);
        }

        int age = Calendar.getInstance().get(Calendar.YEAR) - Integer.valueOf(birthyear);

        viewHolder.textView_username.setText(App.context.getResources().getString(R.string.user_profile_text_name, firstName, lastName, age));
        viewHolder.textView_phonenumber.setText(phoneNumber);
        viewHolder.textView_usercity.setText(zipcode + " " + city);
        viewHolder.ratingBar_userRating.setRating((float) 4.5);
        // viewHolder.textView_userHabits.setText(habits);
        viewHolder.textView_userEmail.setText(email);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Log.e(TAG, e.toString());
                Log.e(TAG, "image picker error");
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Log.i(TAG, "onImagePicked");
                //Handle the image
                Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath());
                viewHolder.imageView_profilePicture.setImageBitmap(bitmap);

                String bitmapString = StringUtils.getStringFromBitmap(bitmap, 100);
                UserService.getInstance().updateProfilePicture(bitmapString);
                UserProfile.getInstance().setProfilePicture(bitmapString);

            }
        });


    }

    private void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 100);
            cropIntent.putExtra("aspectY", 62);
            //indicate output X and Y
            //cropIntent.putExtra("outputX", 1000);
            //cropIntent.putExtra("outputY", 620);

            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, 55);
        } catch (Exception exception) {
            //display an error message
//            String
            String errorMessage = exception.getMessage();
            Log.e(TAG, errorMessage);
            errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(App.context, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (onFragmentInteractionListener != null) {
            onFragmentInteractionListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            onFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentInteractionListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
