package com.mannheim.dhbw.wwi13sca.meetneat.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mannheim.dhbw.wwi13sca.meetneat.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by marce on 20.06.2016.
 */
public class ProfileDialog extends DialogFragment {


    @Bind(R.id.profile_username)TextView hostName;
    @Bind(R.id.profile_userPicture) ImageView hostProfilePicture;
    @Bind(R.id.profile_userRatingbar) RatingBar hostRatingBar;
    @Bind(R.id.profile_phonenumber) TextView hostPhoneNumber;

    public ProfileDialog() {
    // Empty constructor is required for DialogFragment
    // Make sure not to add arguments to the constructor
    // Use `newInstance` instead as shown below
    }

    public static ProfileDialog newInstance(String hostName, String hostProfilePicture, float hostRating, String hostPhoneNumber) {
        ProfileDialog frag = new ProfileDialog();
        Bundle args = new Bundle();
        args.putString("hostName", hostName);
        // args.putString("hostProfilePicture", hostProfilePicture);
        args.putFloat("hostRating", hostRating);
        args.putString("hostPhoneNumber", hostPhoneNumber);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dialog_fragment_profile, container);
        ButterKnife.bind(this, rootView);

        hostName.setText(getArguments().getString("hostName"));
        //hostProfilePicture.setImageBitmap(StringUtils.getBitmapFromString(getArguments().getString("hostName")));
        hostRatingBar.setRating(5.0f);
        // hostPhoneNumber.setText(getArguments().getString("hostPhoneNumber"));
        hostPhoneNumber.setText("0123 456789");

        hostPhoneNumber.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("smsto:" + hostPhoneNumber.getText().toString().trim());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(i, ""));
            }
        });

        return rootView;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");


        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
