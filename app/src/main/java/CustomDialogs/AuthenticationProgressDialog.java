package CustomDialogs;

import android.app.ProgressDialog;
import android.content.Context;

import com.mannheim.dhbw.wwi13sca.meetneat.R;

public class AuthenticationProgressDialog extends ProgressDialog {
    public AuthenticationProgressDialog(Context context) {
        super(context);
        setMessage(context.getString(R.string.dialog_message_authenticate));
        setCancelable(false);
    }

    public AuthenticationProgressDialog(Context context, String message) {
        super(context);
        setMessage(message);
        setCancelable(false);
    }
}
